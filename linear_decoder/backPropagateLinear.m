function [DW, Db] = backPropagateLinear(Y, W, A, rho_hat, rho, beta)
% See backPropagate.m for argument descriptions
% Backpropagate with linear output layer

N = length(A);
M = size(Y,2);
DW = {}; Db = {};

% Output layer error terms (note indexing from 1:N-1, not 2:N)
Db{N-1} = -(Y - A{N}); % Change for linear activation function
DW{N-1} = Db{N-1}*A{N-1}'/M;

% All the rest are hidden layers
for l=N-1:-1:2
   sparsity_term = beta*repmat((-rho./rho_hat + (1-rho)./(1-rho_hat)), [1,M]);
   Db{l-1} = (W{l}'*Db{l} + sparsity_term) .* (A{l}.*(1 - A{l})); % f'
   DW{l-1} = Db{l-1}*A{l-1}'/M;
end

for l = 1:N-1
   Db{l} = sum(Db{l},2)/M;
end

end
