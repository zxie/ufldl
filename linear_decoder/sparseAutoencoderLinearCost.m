function [cost,grad,features] = sparseAutoencoderLinearCost(theta, visibleSize, hiddenSize, ...
                                                            lambda, sparsityParam, beta, data)
% -------------------- YOUR CODE HERE --------------------
% Instructions:
%   Copy sparseAutoencoderCost in sparseAutoencoderCost.m from your
%   earlier exercise onto this file, renaming the function to
%   sparseAutoencoderLinearCost, and changing the autoencoder to use a
%   linear decoder.
% -------------------- YOUR CODE HERE --------------------                                    

%FIXME: Assumes 3 layers
W1 = reshape(theta(1:hiddenSize*visibleSize), hiddenSize, visibleSize);
W2 = reshape(theta(hiddenSize*visibleSize+1:2*hiddenSize*visibleSize), visibleSize, hiddenSize);
b1 = theta(2*hiddenSize*visibleSize+1:2*hiddenSize*visibleSize+hiddenSize);
b2 = theta(2*hiddenSize*visibleSize+hiddenSize+1:end);

% Cost and gradient variables (your code needs to compute these values). 
% Here, we initialize them to zeros. 
cost = 0;
W1grad = zeros(size(W1)); 
W2grad = zeros(size(W2));
b1grad = zeros(size(b1)); 
b2grad = zeros(size(b2));

W={}; W{1} = W1; W{2} = W2;
b={}; b{1} = b1; b{2} = b2;
M = size(data, 2);  % Number of training examples
N = length(W)+1;    % Number of layers
activations = forwardPropagate(data, W, b);
%% Linear decoder now so modify third layer activations
activations{3} = W2*activations{2} + ...
    repmat(b2, [1, size(activations{2},2)]);
rho_hat = 1/M*sum(activations{2}, 2);

off = activations{3} - data;
mse = sum(sum(off.^2, 1))/M;
cost_mse = mse/2;

%% Modified backPropagate for linear output activation as well
[DW, Db] = backPropagateLinear(data, W, activations, rho_hat, sparsityParam, beta);
W1grad = DW{1}; W2grad = DW{2};
b1grad = Db{1}; b2grad = Db{2};

% Weight decay term; augment W gradients

cost_decay = 0;
for k=1:N-1
    cost_decay = cost_decay + sum(sum(W{k}.*W{k}));
end
cost_decay = lambda/2 * cost_decay;

W1grad = W1grad + lambda*W{1};
W2grad = W2grad + lambda*W{2};

% Sparsity cost penalty

cost_sparse = beta*sum(kl_divergence(sparsityParam, rho_hat));

cost = cost_mse + cost_decay + cost_sparse;

%-------------------------------------------------------------------
% After computing the cost and gradient, we will convert the gradients back
% to a vector format (suitable for minFunc).  Specifically, we will unroll
% your gradient matrices into a vector.



grad = [W1grad(:) ; W2grad(:) ; b1grad(:) ; b2grad(:)];

end

function kld = kl_divergence(p, q)
% Implementation for Bernoulli p, q only
    kld = p.*log(p./q) + (1-p).*log((1-p)./(1-q));
end