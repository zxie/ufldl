function A = forwardPropagate(X, W, b)
% Computes output and activations at each layer of neural network.
% Inputs:
% - X: input matrix such that X(:,k) = input vector k (k=1:M)
% - W: cell array of matrices such that W{l} is weight matrix applied to
%   layer l
% - b: cell array of bias vectors such that b{l} is bias vector of layer l
% Outputs:
% - A: cell array of activations at layers 2:N of network such that A{l}
%   is matrix of activations 1:M, where M is number of training examples,
%   for layer l; A{1} = X
    M = size(X, 2);
    A = {};
    A{1} = X;
    for k=1:length(W)
        A{k+1} = sigmoid(W{k}*A{k} + repmat(b{k}, [1, M]));
    end
end