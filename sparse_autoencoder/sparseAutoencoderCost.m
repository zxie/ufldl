function [cost,grad] = sparseAutoencoderCost(theta, visibleSize, hiddenSize, ...
                                             lambda, sparsityParam, beta, data)

% visibleSize: the number of input units (probably 64) 
% hiddenSize: the number of hidden units (probably 25) 
% lambda: weight decay parameter
% sparsityParam: The desired average activation for the hidden units (denoted in the lecture
%                           notes by the greek alphabet rho, which looks like a lower-case "p").
% beta: weight of sparsity penalty term
% data: Our 64x10000 matrix containing the training data.  So, data(:,i) is the i-th training example. 
  
% The input theta is a vector (because minFunc expects the parameters to be a vector). 
% We first convert theta to the (W1, W2, b1, b2) matrix/vector format, so that this 
% follows the notation convention of the lecture notes. 

%FIXME: Assumes 3 layers
W1 = reshape(theta(1:hiddenSize*visibleSize), hiddenSize, visibleSize);
W2 = reshape(theta(hiddenSize*visibleSize+1:2*hiddenSize*visibleSize), visibleSize, hiddenSize);
b1 = theta(2*hiddenSize*visibleSize+1:2*hiddenSize*visibleSize+hiddenSize);
b2 = theta(2*hiddenSize*visibleSize+hiddenSize+1:end);

% Cost and gradient variables (your code needs to compute these values). 
% Here, we initialize them to zeros. 
cost = 0;
W1grad = zeros(size(W1)); 
W2grad = zeros(size(W2));
b1grad = zeros(size(b1)); 
b2grad = zeros(size(b2));

%% ---------- YOUR CODE HERE --------------------------------------
%  Instructions: Compute the cost/optimization objective J_sparse(W,b) for the Sparse Autoencoder,
%                and the corresponding gradients W1grad, W2grad, b1grad, b2grad.
%
% W1grad, W2grad, b1grad and b2grad should be computed using backpropagation.
% Note that W1grad has the same dimensions as W1, b1grad has the same dimensions
% as b1, etc.  Your code should set W1grad to be the partial derivative of J_sparse(W,b) with
% respect to W1.  I.e., W1grad(i,j) should be the partial derivative of J_sparse(W,b) 
% with respect to the input parameter W1(i,j).  Thus, W1grad should be equal to the term 
% [(1/m) \Delta W^{(1)} + \lambda W^{(1)}] in the last block of pseudo-code in Section 2.2 
% of the lecture notes (and similarly for W2grad, b1grad, b2grad).
% 
% Stated differently, if we were using batch gradient descent to optimize the parameters,
% the gradient descent update to W1 would be W1 := W1 - alpha * W1grad, and similarly for W2, b1, b2. 
% 

% 1/2 mean squared-error term and corresponding gradients
% Gradients also include sparsity penalty

W={}; W{1} = W1; W{2} = W2;
b={}; b{1} = b1; b{2} = b2;
M = size(data, 2);  % Number of training examples
N = length(W)+1;    % Number of layers
activations = forwardPropagate(data, W, b);
rho_hat = 1/M*sum(activations{2}, 2);

off = activations{3} - data;
mse = sum(sum(off.^2, 1))/M;
cost_mse = mse/2;

[DW, Db] = backPropagate(data, W, activations, rho_hat, sparsityParam, beta);
W1grad = DW{1}; W2grad = DW{2};
b1grad = Db{1}; b2grad = Db{2};

% Weight decay term; augment W gradients

cost_decay = 0;
for k=1:N-1
    cost_decay = cost_decay + sum(sum(W{k}.*W{k}));
end
cost_decay = lambda/2 * cost_decay;

W1grad = W1grad + lambda*W{1};
W2grad = W2grad + lambda*W{2};

% Sparsity cost penalty

cost_sparse = beta*sum(kl_divergence(sparsityParam, rho_hat));

cost = cost_mse + cost_decay + cost_sparse;

%-------------------------------------------------------------------
% After computing the cost and gradient, we will convert the gradients back
% to a vector format (suitable for minFunc).  Specifically, we will unroll
% your gradient matrices into a vector.



grad = [W1grad(:) ; W2grad(:) ; b1grad(:) ; b2grad(:)];

end

function kld = kl_divergence(p, q)
% Implementation for Bernoulli p, q only
    kld = p.*log(p./q) + (1-p).*log((1-p)./(1-q));
end

