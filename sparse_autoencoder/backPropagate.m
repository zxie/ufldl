% Currently does sum/average over examples as well...
function [DW, Db] = backPropagate(Y, W, A, rho_hat, rho, beta)
% Computes partial derivatives of cost wrt W^l and b^l
% Assumes sigmoid function being used for closed form of f'(z_i^l)--easy
% to replace with closed form for tanh, perhaps make argument later %FIXME
% Inputs:
% - Y: matrix such that Y(:,k) is "label" k in training examples (k=1:M)
% - W: cell array of matrices such that W{l} is weight matrix applied to
%   layer l
% - A: activations computed using forwardPropagate function
% - rho_hat: average activation of hidden units
% - rho: desired average activation of hidden units
% - beta: weight of sparsity penalty term
% Outputs:
% - DW: cell array of matrices dW such that dW(i,j) is partial derivative
%   of cost wrt W_ij (cell k corresponds to layer k+1, layers 2:N)
%   summed over all training examples and averaged
% - Db: cell array of vectors db such that db(i) is partial derivative
%   of cost wrt b_i (cell k corresponds to layer k+1, layers 2:N)
%   summed over all training examples and averaged

N = length(A);
M = size(Y,2);
DW = {}; Db = {};

% Output layer error terms (note indexing from 1:N-1, not 2:N)
Db{N-1} = -(Y - A{N}) .* (A{N}.*(1 - A{N})); % f'
DW{N-1} = Db{N-1}*A{N-1}'/M;

% All the rest are hidden layers
for l=N-1:-1:2
   sparsity_term = beta*repmat((-rho./rho_hat + (1-rho)./(1-rho_hat)), [1,M]);
   Db{l-1} = (W{l}'*Db{l} + sparsity_term) .* (A{l}.*(1 - A{l})); % f'
   DW{l-1} = Db{l-1}*A{l-1}'/M;
end

for l = 1:N-1
   Db{l} = sum(Db{l},2)/M;
end

end
