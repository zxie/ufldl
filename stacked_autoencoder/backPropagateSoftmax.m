function [DW, Db] = backPropagateSoftmax(Y, W, A, theta)
% Implementation of backpropagation for fine-tuning assuming softmax
% regression
% Refer to backPropagate.m for argument descriptions

N = length(A);
M = size(Y,2);
DW = {}; Db = {};

% Softmax layer error terms (note indexing from 1:N-1, not 2:N)

% Matrix of conditional probabilities
P = theta*A{N};
P = bsxfun(@minus, P, max(P, [], 1)); 
P = exp(P);
P = bsxfun(@rdivide, P, sum(P));

Db{N-1} = -theta'*(Y-P) .* (A{N}.*(1 - A{N})); % f'
DW{N-1} = Db{N-1}*A{N-1}'/M;

% All the rest are hidden layers
for l=N-1:-1:2
   Db{l-1} = (W{l}'*Db{l}) .* (A{l}.*(1 - A{l})); % f'
   DW{l-1} = Db{l-1}*A{l-1}'/M;
end

for l = 1:N-1
   Db{l} = sum(Db{l},2)/M;
end

end
